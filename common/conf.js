/** システム定数クラス */
// 追加要素設定 PDA表示の追加要素
const additionalcConf = {
    'peace': [{'div': 'rule', 'name':'ルール', 'info': '追加要素（ルール）'},],
    'peace2': [{'div': 'rule', 'name':'ルール', 'info': '追加要素（ルール）'},
               {'div': 'stage', 'name':'ステージ', 'info': '追加要素（ステージ）'},],
    'sss': [{'div': 'keyword', 'name':'キーワード', 'info': 'キーワード'},]
}
const sysConf = {
    'peace': {'advance': 1, 'name': 'キラークイーン peace' },
    'peace2': {'advance': 1, 'name': 'キラークイーン peace2', 
               'logurl': 'http://ciel.moo.jp/cheat/sow.cgi?vid=87', 
               'wikiurl': 'http://wolfsnest.sakura.ne.jp/KillerQueen/?%B4%EB%B2%E8%2F%A5%AD%A5%E9%A1%BC%A5%AF%A5%A4%A1%BC%A5%F3peace2',},
    'demo': {'advance': 0, 'name': 'キラークイーン XXX', 
             'logurl': 'http://ciel.moo.jp/cheat/sow.cgi', 
             'wikiurl': 'http://wolfsnest.sakura.ne.jp/KillerQueen/', },
    'sss': {'advance': 1, 'name': 'キラークイーン SSS', 
            'logurl': 'http://ciel.moo.jp/cheat/sow.cgi?vid=57',
            'wikiurl': 'http://melon-cirrus.sakura.ne.jp/wiki/?%A1%DA%C6%C3%BC%EC%A5%AC%A5%C1%A1%DB%A5%AD%A5%E9%A1%BC%A5%AF%A5%A4%A1%BC%A5%F3SSS', }
}
const adminConf = {
    "scopelist":['demo','peace','peace2', 'sss', 'admin',],
    "authtypelist":[{'name': 'admin', 'value': 0}, {'name': 'Game Master', 'value': 1},{'name': 'Player', 'value': 2},]
}
exports.getAdminConf = adminConf;
exports.getAdditionalConf = function(scope){
    return additionalcConf[scope];
}
exports.getSysConf = function(scope){
    return sysConf[scope];
}