const pg = require('pg');
require('dotenv').config();
const pool = new pg.Pool ({
  host: process.env.ENV_HOST,
  database: process.env.ENV_DB,
  user: process.env.ENV_USER,
  port: 5432,
  password: process.env.ENV_PASSWORD,
  ssl: true,
});

exports.pool = pool;
exports.setQueryStr = function(query, args){
  args.forEach(element => {
    query = query.replace('?', element);
  });
  return query;
};
//auth
exports.selectUserByAuth = 'SELECT scope, user_id, password, auth_type , user_name FROM m_user '
 + 'WHERE user_id = $1 and valid = 1;';
//admin
exports.selectUserAll = 'SELECT scope, user_id, password, auth_type , user_name FROM m_user '
 + 'WHERE valid = 1 ORDER BY scope, user_id;';
exports.selectUserByScope = 'SELECT scope, user_id, password, auth_type , user_name FROM m_user '
 + 'WHERE scope= $1 and valid = 1 ORDER BY auth_type, user_id;';
exports.selectUserByPk = 'SELECT scope, user_id, password, auth_type, user_name, valid FROM m_user WHERE scope = $1 and user_id = $2;';
exports.updateUser = 'UPDATE m_user SET password = $3, auth_type=$4, user_name = $5, valid = $6 '
+ 'WHERE scope = $1 and user_id = $2;';
exports.inseretUser = 'INSERT INTO m_user (scope, user_id, password, auth_type, user_name, valid) '
 + 'VALUES($1, $2, $3, $4, $5, $6)';
 //player
exports.selectPlayer = 'SELECT player_id, role, role_disp, identify1, identify2, info, now_hist_no, valid_from, death, cause'
 + ' from t_player WHERE scope = $1 and user_id = $2;';
exports.selectPlayerCond = 'SELECT cond , exp, valid from t_player_cond WHERE scope = $1 and player_id = $2 and valid = 1'
 + ' order by cond_no;';
exports.selectPlayerTool = 'SELECT name , info, exp, valid from t_player_tool WHERE scope = $1 and player_id = $2 and valid = 1'
+ ' order by tool_no;';
exports.selectPlayerAdditional = 'SELECT add_no, title, info, exp, valid from t_player_additional WHERE scope = $1 and player_id = $2 and add_div = $3'
+ ' and valid = 1 order by add_no;';

exports.selectPlayerHistory = 'SELECT scope, player_id, hist_no, role, role_disp, identify1, identify2, info, user_id'
 + ', expire_datetime, expire_progress_date, death, cause'
 + ' from t_player_history WHERE scope = $1 and player_id = $2 and hist_no = $3;';
exports.selectPlayerCondHistory = 'SELECT scope, player_id, hist_no, cond_no, valid, cond, exp '
 + ' from t_player_cond_history WHERE scope = $1 and player_id = $2 and hist_no = $3 and valid = 1'
 + ' order by cond_no;';
exports.selectPlayerToolHistory = 'SELECT scope, player_id, hist_no, tool_no, valid, name, info, exp '
+ ' from t_player_tool_history WHERE scope = $1 and player_id = $2 and hist_no = $3 and valid = 1'
+ ' order by tool_no;';
exports.selectPlayerAdditionalHistory = 'SELECT scope, player_id, hist_no, add_div, add_no, valid, title, info, exp '
+ ' from t_player_additional_history WHERE scope = $1 and player_id = $2 and hist_no = $3 and add_div = $4 and valid = 1'
+ ' and valid = 1 order by add_no;';
//GM
exports.selectPlayers = 'SELECT p.player_id, role, role_disp, identify1, identify2, p.info, p.user_id'
 + ', now_hist_no, valid_from, c.cond, death, cause, u.user_name'
 + ' from t_player p  left join t_player_cond c on p.scope = c.scope and p.player_id = c.player_id and c.cond_no = 1 and c.valid = 1'
 + ' left join m_user u on p.scope = u.scope and p.user_id = u.user_id '
 + ' WHERE p.scope = $1 order by p.player_id;';
exports.selectUsers = 'SELECT user_id, user_name from m_user WHERE scope = $1 and auth_type = 2 and valid = 1 order by user_id;';
exports.updatePlayer = 'UPDATE t_player SET  identify1 = $1,  identify2 = $2 , user_id = $3, info = $4, now_hist_no = $7, valid_from = $8'
                     + ', death = $9, cause = $10'
                     + 'WHERE scope = $5 and player_id = $6;';
exports.updateSysProgress = 'UPDATE m_system_progress SET progress_date = $1 , end_date = $3 WHERE scope = $2 ;';
exports.selectPlayerByPid = 'SELECT player_id, role, role_disp, identify1, identify2, info, p.user_id, now_hist_no, valid_from, u.user_name'
 + ', death, cause'
 + ' from t_player p left join m_user u on p.scope = u.scope and p.user_id = u.user_id  WHERE p.scope = $1 and player_id = $2;';
exports.selectPlayerCondAll = 'SELECT scope, player_id, cond_no, valid, cond, exp from t_player_cond WHERE scope = $1 and player_id = $2 '
 + ' order by cond_no;';
exports.selectPlayerToolAll = 'SELECT scope, player_id, tool_no, valid, name, info, exp from t_player_tool WHERE scope = $1 and player_id = $2 '
+ ' order by tool_no;';
exports.selectPlayerAdditionalAll = 'SELECT scope, player_id, add_div, add_no, valid, title, info, exp from t_player_additional WHERE scope = $1 and player_id = $2 and add_div = $3'
+ ' order by add_no;';
exports.deletePlayerCond = 'DELETE FROM t_player_cond WHERE scope = $1 and player_id = $2;';
exports.deletePlayerTool = 'DELETE FROM t_player_tool WHERE scope = $1 and player_id = $2;';
exports.deletePlayerAdditional = 'DELETE FROM t_player_additional WHERE scope = $1 and player_id = $2 and add_div = $3;';
exports.insertPlayerCond = 'INSERT INTO t_player_cond (scope, player_id, cond_no, valid, cond, exp) VALUES '
                     + '($1, $2, $3, $4, $5, $6);';
exports.insertPlayerTool = 'INSERT INTO t_player_tool (scope, player_id, tool_no, valid, name, info, exp) VALUES '
                    + '($1, $2, $3, $4, $5, $6, $7);';
exports.insertPlayerAdditional = 'INSERT INTO t_player_additional (scope, player_id, add_div, add_no, valid, title, info, exp) VALUES '
                    + '($1, $2, $3, $4, $5, $6, $7, $8);';
exports.insertPlayerHist = 'INSERT INTO t_player_history '
                          + '(scope, player_id, hist_no, role, role_disp, identify1, identify2, info, user_id, expire_datetime'
                          + ', expire_progress_date, valid_from, death, cause) VALUES '
                          + '($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14);';
exports.insertPlayerCondHist = 'INSERT INTO t_player_cond_history '
                          + '(scope, player_id, hist_no, cond_no, valid, cond, exp) VALUES '
                          + '($1, $2, $3, $4, $5, $6, $7);';
exports.insertPlayerToolHist = 'INSERT INTO t_player_tool_history '
                          + '(scope, player_id, hist_no, tool_no, valid, name, info, exp) VALUES '
                          + '($1, $2, $3, $4, $5, $6, $7, $8);';//COMMON
exports.insertPlayerAddHist = 'INSERT INTO t_player_additional_history '
                          + '(scope, player_id, hist_no, add_div, add_no, valid, title, info, exp) VALUES '
                          + '($1, $2, $3, $4, $5, $6, $7, $8, $9);';//COMMON
exports.selectSysProgress = 'SELECT progress_date, end_date from m_system_progress WHERE scope = $1 ;';
// GM 日替わり処理断面作成
exports.insertPlayerDwh = 'INSERT INTO t_player_dwh ("scope",sys_date,player_id,"role",role_disp,identify1,identify2,cond,info,death,cause,hist_no, user_name) VALUES '
                     + '($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13);';
//epilogue
exports.selectPlayerDwh = 'select scope, sys_date, player_id, role, role_disp, identify1, identify2, cond, info, death, cause, hist_no, user_name'
+ ' from t_player_dwh where scope = $1 and sys_date = $2 order by player_id;'
exports.selectEpiPlayers = 'SELECT player_id, role, role_disp, identify1, identify2, info, p.user_id, now_hist_no, u.user_name'
+ ', death, cause'
+ ' from t_player p left join m_user u on p.scope = u.scope and p.user_id = u.user_id  WHERE p.scope = $1 order by player_id;';
exports.selectPlayerHistoryCnt = 'SELECT COUNT(*) as cnt from t_player_history WHERE scope = $1 and player_id = $2;';
exports.selectPlayerHistoryEpi = 'SELECT p.scope, player_id, hist_no, role, role_disp, identify1, identify2, info, p.user_id, expire_datetime, expire_progress_date, u.user_name'
 + ', death, cause'
 + ' from t_player_history p left join m_user u on p.scope = u.scope and p.user_id = u.user_id WHERE p.scope = $1 and p.player_id = $2 and hist_no = $3;';