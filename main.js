const express = require("express");
const app = express();
//環境変数
require('dotenv').config();
//ルーティング
var admin = require('./routes/admin');
var gamemaster = require('./routes/gamemaster');
var player = require('./routes/player');
var epilogue = require('./routes/epilogue');
var login = require('./routes/auth');
var sgrouter = require('./spacegollilla_log/router');
//テンプレートエンジン設定
const path = require('path');
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');


//静的ファイル格納ディレクトリ設定
app.use(express.static('./public'));
//セッション管理
var cookieParser = require('cookie-parser');
var passport = require('passport');
var session = require('express-session');


app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(session({ resave:false,saveUninitialized:false, secret: 'passport test' }));
app.use(passport.initialize());
app.use(passport.session());

//postgres
var db = require('./common/db')

app.get('/', (req, res, next) => {
    //res.redirect('/login');
    res.render('./',{id: '', test: 'aaa'});
  });
  app.get('/error', (req, res, next) => {
    res.render('./err500', { 'error': "" });
  });
  
app.use('/aaa', express.static('./public/index2.html'))
//app.use('/spacegollilla', express.static('./spacegollilla_log/main_log.html'))
app.use('/spacegollilla', sgrouter);
app.use('/admin', admin);
app.use('/gamemaster', gamemaster);
app.use('/player', player);
app.use('/epilogue', epilogue);
app.use('/login', login);
app.get('/logout', function(req, res) {
    req.logout();
    res.redirect('/login');
});
//エラーハンドリング
app.use( function( req, res, next ){
    res.status( 404 ); //. 404 エラー
    res.render( 'err404', { 'path': req.path } ); //. 404 エラーが発生したパスをパラメータとして渡す
});
app.use( function( err, req, res, next ){
    res.status( 500 ); //. 500 エラー
    console.log(err);
    res.render( 'err500', { 'error': err } ); //. 500 エラーの内容をパラメータとして渡す
});
var server = app.listen(process.env.PORT || 3000, function(){
    console.log("Node.js is listening to PORT:" + server.address().port);
});