$(function(){
    $('#btnRegist').on('click', regUser);
    $('#btnSearch').on('click', function() {
        $('#searchForm').submit();
    });
    $('#btnNewRegist').on('click', function() {
        window.location.href = $(this).data('url');
    });

    $('#btnBack').on('click', function(){
        window.history.back();
    });
    //ユーザーins/upd
    function regUser(){
        $('.uk-button').prop('disabled', true);
        $('.span_msg').text('実行中…').addClass('uk-text-primary');
        var pid = location.pathname.substring(location.pathname.lastIndexOf('/') + 1);
        //TODO 入力チェック
        var data = {};
        var page = $('#main');
        data.scope = page.find('.sel_scope').val();
        data.auth_type = page.find('.sel_auth_type').val();
        data.user_id = page.find('.txt_userid').val()
        data.password = page.find('.txt_password').val();
        data.user_name = page.find('.txt_username').val();
        data.valid = page.find('input[name="valid"]:checked').val();
        data.isNew = page.find('input[name="isNew"]').val();
        console.log(JSON.stringify(data));
        $.ajax({
            url: 'regUser',
            type: 'POST',
            data: JSON.stringify({'data': data }),
            dataType: "json",
            contentType: "application/json",
        }).done(function(data){ 
            UIkit.notification({
                id: 'notification',
                message: data.message,
                status: 'success',
                pos: 'bottom-center',
                timeout: 5000,
            });
        }).fail(function(e){
            console.log(JSON.stringify(e))
            UIkit.notification({
                id: 'notification',
                message: e.responseJSON ? e.responseJSON.message : "エラー",
                status: 'danger',
                pos: 'bottom-center',
                timeout: 5000,
            });
        }).always(function(){
            $('.span_msg').text('');
            $('.uk-button').prop('disabled', false);
        });
    }
});
//入力前後値が異なる場合はfalseを返却
function chkOldval(obj, key){
    var oldval = $(obj).data(key);
    var newval = $(obj).find('.' + key).val();
    return (oldval == newval);
}
