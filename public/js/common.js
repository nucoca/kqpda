$(function(){
    var advance = $('body').data('advance');
    if(!advance) $('.ele_advance').hide();
    //共通 外部リンク処理
    $('.a_link_blank').on('click', function(){
        var url = $(this).data('url')
        window.open(url);
    });
    // 閉じるボタン
    $('.btn_close').on('click', function() {
        window.open('about:blank','_self').close();
    })
});