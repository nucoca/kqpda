$(function(){
    $('#savePlayers').on('click', save_player_data);
    $('#modalCommitComfirm').on('click', commit_sysdate);
    $('#modalEndCommitComfirm').on('click', commit_end);
    //終了ボタン制御
    var sysdate = $('#modalCommitComfirm').data('sysdate') ? $('#modalCommitComfirm').data('sysdate') : 0;
    if(sysdate > 0) $('#btnEndCommit').removeClass('uk-hidden');
    //option行表示ボタン制御
    $('#a_show').on('click', function(){
        var icon = $(this).attr('uk-icon')
        if(icon == 'icon: pencil') {
            $('.col_opt_th').removeClass('uk-width-small');
            $('.col_opt_th').addClass('uk-width-medium');
            $(this).attr('uk-icon', 'icon: close')
        } else{
            $('.col_opt_th').removeClass('uk-width-medium');
            $('.col_opt_th').addClass('uk-width-small');
            $(this).attr('uk-icon', 'icon: pencil')
        }
    });

    //ゲーム開始処理
    function commit_sysdate(){
        var sysdate = $('#modalCommitComfirm').data('sysdate') ? $('#modalCommitComfirm').data('sysdate') : 0;
        var scope = location.pathname.substring(location.pathname.lastIndexOf('/') + 1);
        var defaultmessage = $('#modalCommitText').text();
        $('.uk-button').prop('disabled', true);
        $('#modalCommitText').text('実行中………');
        $.ajax({
            url: scope + '/commit',
            type: 'POST',
            data: JSON.stringify({'sysdate': sysdate, }),
            dataType: "json",
            contentType: "application/json",
        }).done(function(data){ 
            var newdate = data.sysdate;
            //モーダルのテキスト書き換え
            $('#modalCommitComfirm').data('sysdate', newdate);
            $('#modalEndCommitComfirm').data('sysdate', newdate);
            $('#spanSysdate').text('ゲーム ' + newdate + '日目');
            $('#modalCommitTitle').text('ゲーム進行');
            $('#modalCommitText').text('ゲームの日付を進めます。よろしいですか。');
            $('#btnEndCommit').removeClass('uk-hidden');
            $('.user_id').prop('disabled', true);
            UIkit.notification({
                id: 'notification',
                message: '処理結果:OK',
                status: 'success',
                pos: 'bottom-center',
                timeout: 5000,
            });
        }).fail(function(e){
            console.log(e);
            $('#modalCommitText').text(defaultmessage);
            UIkit.notification({
                id: 'notification',
                message: e.responseJSON.message,
                status: 'danger',
                pos: 'bottom-center',
                timeout: 5000,
            });
        }).always(function(){
            $('.uk-button').prop('disabled', false);
            UIkit.modal('#modalCommit').hide();
        });
    }
    //プレイヤーデータ編集
    function save_player_data (){
        $('.uk-button').prop('disabled', true);
        var rows = $('#players').find('tr');
        var arr = [];
        for(i = 0; i < rows.length; i++){
            var row = rows[i];
            //ヘッダはスルー
            if(i == 0) continue;
            var death = $(row).find('.death').prop('checked') ? 1 : 0;
            if(chkOldval(row, 'identify1') && chkOldval(row, 'identify2')  && chkOldval(row, 'user_id')
               && chkOldval2(row, 'death', death) && chkOldval(row, 'cause') && chkOldval(row, 'info') ) continue;
            var obj = new Object();
            obj.player_id = $(row).data('id');
            obj.identify1 = $(row).find('.identify1').val();
            obj.identify2 = $(row).find('.identify2').val();
            obj.user_id = $(row).find('.user_id').val();
            obj.user_name = $(row).find('.user_id option:selected').text();
            obj.death = death;
            obj.cause = $(row).find('.cause').val();
            obj.info = $(row).find('.info').val();
            arr.push(obj);
        }
        var scope = location.pathname.substring(location.pathname.lastIndexOf('/') + 1);
        var sysdate = $('#modalCommitComfirm').data('sysdate');
        console.log(JSON.stringify(arr));
        $.ajax({
            url: scope + '/savePlayer',
            type: 'POST',
            data: JSON.stringify({'players': arr, 'sysdate': sysdate, }),
            dataType: "json",
            contentType: "application/json",
        }).done(function(data){ 
            for(i = 0; i < rows.length; i++){
                if(i == 0) continue;
                var row = rows[i];
                for(k = 0; k < arr.length; k++){
                    if($(row).data('id') != arr[k].player_id ) continue;
                    $(row).data('identify1', arr[k].identify1)
                    $(row).data('identify2', arr[k].identify2)
                    $(row).data('user_id', arr[k].user_id)
                    $(row).data('death', arr[k].death)
                    $(row).data('cause', arr[k].cause)
                    $(row).data('info', arr[k].info)
                    break;
                }
            }
            UIkit.notification({
                id: 'notification',
                message: data.message,
                status: 'success',
                pos: 'bottom-center',
                timeout: 5000,
            });
        }).fail(function(e){
            UIkit.notification({
                id: 'notification',
                message: e.responseJSON ? e.responseJSON.message : "エラー",
                status: 'danger',
                pos: 'bottom-center',
                timeout: 5000,
            });
        }).always(function(){
            $('.uk-button').prop('disabled', false);
        });
    }

    //ゲーム終了処理
    function commit_end(){
        var sysdate = $('#modalCommitComfirm').data('sysdate') ? $('#modalCommitComfirm').data('sysdate') : 0;
        var scope = location.pathname.substring(location.pathname.lastIndexOf('/') + 1);
        var defaultmessage = $('#modalEndCommitText').text();
        $('.uk-button').prop('disabled', true);
        $('#modalEndCommitText').text('実行中………');
        var data = {'sysdate': sysdate};
        var rows = $('#players').find('tr');
        var arr = [];
        for(i = 0; i < rows.length; i++){
            var row = rows[i];
            //ヘッダはスルー
            if(i == 0) continue;
            arr.push($(row).data('id'));
        }
        data.pids = arr;
        $.ajax({
            url: scope + '/endcommit',
            type: 'POST',
            data: JSON.stringify(data),
            dataType: "json",
            contentType: "application/json",
        }).done(function(data){ 
            if(!data.result) {
                window.location.href = "/error";
            }
            window.location.href = "/epilogue/" + scope;
        }).fail(function(e){
            console.log(e);
            $('#modalEndCommitText').text(defaultmessage);
            UIkit.notification({
                id: 'notification',
                message: e.responseJSON ? e.responseJSON.message : "エラー",
                status: 'danger',
                pos: 'bottom-center',
                timeout: 5000,
            });
        }).always(function(){
            $('.uk-button').prop('disabled', false);
            UIkit.modal('#modalEndCommit').hide();
        });
    }
});
//入力前後値が異なる場合はfalseを返却
function chkOldval(obj, key){
    var oldval = $(obj).data(key);
    var newval = $(obj).find('.' + key).val()
    return (getValidVal(oldval, !isNaN(newval)) == newval);
}
function chkOldval2(obj, key, val){
    var oldval = $(obj).data(key);
    //if(getValidVal(oldval, !isNaN(val)) != val) console.log(key + " : " + getValidVal(oldval) + " : " + val)
    return (getValidVal(oldval, !isNaN(val)) == val);
}
// 比較に有効な値を返却
function getValidVal(val, isNum) {
    //数値 0の場合はそのまま返却
    if(val == 0) return val;
    //undefinedの場合は空文字または0返却
    if(!val && isNum) return 0;
    if(!val) return "";
    //上記以外はそのままの値
    return val;
}