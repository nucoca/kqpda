$(function(){
    var advance = $('body').data('advance');
    if(!advance) $('.ele_advance').hide();
    $('#btnSave').on('click', save_player_datail);
    $('#btnPreview').on('click', preview);
    //TODO 戻る時の再検索
    $('#btnBack').on('click', function(){window.location.href = "/gamemaster/" + $(this).data('scope')});
    // 行追加ボタン
    $('#btnCondAdd').on('click', makeCondRow).data('cnt', $('.cond_row').length);
    $('#btnToolAdd').on('click', makeToolRow).data('cnt', $('.tool_row').length);
    $('.btnAdditionalAdd').on('click', makeAddRow).each(function(idx, el){
        var div = $(el).data('additionaldiv');
        $(el).data('cnt', $('.add_' + div +'_row').length);
    });

    $('.cond_main, .tool_main, .add_main').find('.uk-input').attr('maxlength', 50);
    $('.cond_main, .tool_main, .add_main').find('.uk-textarea').attr('maxlength', 500);
    $('.cond_main, .tool_main, .add_main').find('.uk-close-large').css('margin-left', 'auto').on('click', delRow);
    //プレイヤーデータpreview
    function preview (){
        var pData = getData(true);
        var form = $("#prevForm");
        form.find('input[name="jsondata"]').val(JSON.stringify(pData));
        window.open('', 'newWindow');
        form.submit();
    }

    //プレイヤーデータ編集
    function save_player_datail (){
        $('.uk-button').prop('disabled', true);
        $('.span_msg').text('実行中…').addClass('uk-text-primary');
        var pid = location.pathname.substring(location.pathname.lastIndexOf('/') + 1);
        var pData = getData();
        pData.player_id = pid;
        $.ajax({
            url: 'save',
            type: 'POST',
            data: JSON.stringify(pData, ),
            dataType: "json",
            contentType: "application/json",
        }).done(function(data){ 
            UIkit.notification({
                id: 'notification',
                message: data.message,
                status: 'success',
                pos: 'bottom-center',
                timeout: 5000,
            });
        }).fail(function(e){
            UIkit.notification({
                id: 'notification',
                message: e.responseJSON ? e.responseJSON.message : "エラー",
                status: 'danger',
                pos: 'bottom-center',
                timeout: 5000,
            });
        }).always(function(){
            $('.span_msg').text('');
            $('.uk-button').prop('disabled', false);
        });
    }
});
// データ取得
function getData(isPreview) {
    //TODO 入力チェック
    // プレイヤーデータ（親）
    var player = {};
    var page = $('#main');
    player.identify1 = escapeHTML(page.find('.player_identify1').val());
    player.identify2 = escapeHTML(page.find('.player_identify2').val());
    player.info = escapeHTML(page.find('.player_info').val());
    player.user_id = escapeHTML(page.find('.player_userid').data('value'));
    player.death = page.find('.player_death').prop('checked') ? 1 : 0;
    player.cause = escapeHTML(page.find('.player_cause').val());
    if(isPreview) {
        player.user_name = escapeHTML(page.find('.player_user_name').val());
        player.role_disp = escapeHTML(page.find('.player_role_disp').val());
    }
    // 条件、ツール
    var condrow = $.find('.cond_row');
    var condarr = [];
    var toolrow = $.find('.tool_row');
    var toolarr = [];
    for(i = 0; i < condrow.length; i++){
        var row = condrow[i];
        var obj = new Object();
        var rownum = $(row).data('rownum');
        obj.valid = escapeHTML($(row).find('input[name="cond_valid' + rownum + '"]:checked').val());
        obj.cond = escapeHTML($(row).find('.txt_cond').val());
        obj.exp = escapeHTML($(row).find('.txt_condexp').val());
        condarr.push(obj);
    }
    for(i = 0; i < toolrow.length; i++){
        var row = toolrow[i];
        var obj = new Object();
        var rownum = $(row).data('rownum');
        obj.valid = escapeHTML($(row).find('input[name="tool_valid' + rownum + '"]:checked').val());
        obj.name = escapeHTML($(row).find('.txt_toolname').val());
        obj.info = escapeHTML($(row).find('.txt_toolinfo').val());
        obj.exp = escapeHTML($(row).find('.txt_toolexp').val());
        toolarr.push(obj);
    }
    //追加要素がある場合のみ
    var additionaldiv = $.find('.add_main');
    var additional = {};
    if(additionaldiv && additionaldiv.length > 0){
        for(i = 0; i < additionaldiv.length; i++){
            var divitem = additionaldiv[i];
            var divname = $(divitem).data('additionaldiv');
            var addrow = $(divitem).find('.add_row');
            var addarr = [];
            for(k = 0; k < addrow.length; k++){
                var row = addrow[k];
                var obj = new Object();
                var rownum = $(row).data('rownum');
                obj.valid = escapeHTML($(row).find('input[name="add_' + divname + "_valid" + rownum + '"]:checked').val());
                obj.title = escapeHTML($(row).find('.txt_addtitle').val());
                obj.info = escapeHTML($(row).find('.txt_addinfo').val());
                obj.exp = escapeHTML($(row).find('.txt_addexp').val());
                addarr.push(obj);
            }
            additional[divname] = addarr;
        }
    }
    return {'player': player, 'cond': condarr, 'tool':toolarr, 'additional': additional };
}
// エスケープ処理
function escapeHTML (string) {
    if(typeof string !== 'string') {
      return string;
    }
    return string.replace(/[<>]/g, function(match) {
      return {
        '<': '&lt;',
        '>': '&gt;',
      }[match]
    });
}
//データ行削除
function delRow(){
    var target = $(this).data('target') + '_row';
    var closestDiv = $(this).closest('.' + target);
    var btn = closestDiv.parent().next('div').find('button');
    btn.data('cnt', $('.' + target).length - 1);
    $(this).closest('.' + target).remove();
}
//条件行作成
function makeCondRow(){
    var cnt = !$(this).data('cnt') ? 1 : 1 +  $(this).data('cnt');
    var advance = $('body').data('advance');
    var maxcnt = advance ? 5 : 1;
    if(cnt > maxcnt) {
        //一応最大5件までにしておく
        UIkit.notification({message: '登録可能最大値を超えています。'})
        return;
    }
    $(this).data('cnt', cnt);
    var rndstr = makeRndStr();
    var rowdiv = $('<div />').addClass('uk-margin uk-flex uk-flex-column uk-margin-small-left cond_row').data('rownum', 'a' + cnt + rndstr);
    var rdo_cond = makeValidRdo('a' + cnt + rndstr, 'cond');
    var txt_cond = makeInp('txt_cond', '条件');
    var txt_condexp = makeTarea('txt_condexp', '条件補足', 2);
    rowdiv.append($('<hr />'), rdo_cond, txt_cond, txt_condexp);
    $('.cond_main').append(rowdiv);
}
//ツール行作成
function makeToolRow(){
    var cnt = !$(this).data('cnt') ? 1 : 1 +  $(this).data('cnt');
    if(cnt > 5) {
        //一応最大5件までにしておく
        UIkit.notification({message: '登録可能最大値を超えています。'})
        return;
    }
    $(this).data('cnt', cnt);
    var rndstr = makeRndStr();
    var rowdiv = $('<div />').addClass('uk-margin uk-flex uk-flex-column uk-margin-small-left tool_row').data('rownum', 'a' + cnt + rndstr);
    var rdo = makeValidRdo('a' + cnt + rndstr, 'tool');
    var txt_toolname = makeInp('txt_toolname', 'ツール名');
    var txt_toolinfo = makeTarea('txt_toolinfo', 'ツール説明', 5);
    var txt_toolexp = makeTarea('txt_toolexp', 'ツール補足', 2);
    rowdiv.append($('<hr />'), rdo, txt_toolname, txt_toolinfo, txt_toolexp);
    $('.tool_main').append(rowdiv);
}
//追加要素行作成
function makeAddRow(){
    var cnt = !$(this).data('cnt') ? 1 : 1 +  $(this).data('cnt');
    if(cnt > 5) {
        //一応最大5件までにしておく
        UIkit.notification({message: '登録可能最大値を超えています。'})
        return;
    }
    $(this).data('cnt', cnt);
    var addTarget = $(this).data('additionaldiv');
    var addName = $(this).data('additionalname');
    var rndstr = makeRndStr();
    var rowdiv = $('<div />').addClass('uk-margin uk-flex uk-flex-column uk-margin-small-left add_row').data('rownum', 'a' + cnt + rndstr)
                .addClass("add_" + addTarget + "_row");
    var rdo = makeValidRdo('a' + cnt + rndstr, 'add_' + addTarget);
    var txt_title = makeInp('txt_addtitle', addName);
    var txt_info = makeTarea('txt_addinfo', addName + '説明', 5);
    var txt_exp = makeTarea('txt_addexp', addName + '補足', 2);
    rowdiv.append($('<hr />'), rdo, txt_title, txt_info, txt_exp);
    $('.add_' + addTarget + "_main").append(rowdiv);
}
//validフォーム作成
function makeValidRdo(rowid, divname){
    var div = $('<div />').addClass('uk-margin-small uk-flex');
    var rd1 = $('<input type="radio" value="0"/>').addClass('uk-radio').attr('name', divname + '_valid' + rowid);
    var rd2 = $('<input type="radio" value="1" checked/>').addClass('uk-radio').attr('name', divname + '_valid' + rowid);
    var closebtn = $('<button />').addClass('uk-close-large uk-icon uk-close').attr('uk-close', true).data('target', divname);
    div.append($('<label />').addClass('uk-margin-right').append(rd1, $('<span />').text('無効')), 
               $('<label />').addClass('uk-margin-right').append(rd2, $('<span />').text('有効')),
               closebtn.css('margin-left', 'auto').on('click', delRow));
    return div;
}
//input作成
function makeInp(cName, placeholder){
    var div = $('<div />').addClass('uk-margin-small');
    var inp = $('<input />').addClass('uk-input ' + cName).attr('placeholder', placeholder).attr('maxlength', 50);
    return div.append(inp);
}
//textarea作成
function makeTarea(cName, placeholder, rownum){
    var div = $('<div />').addClass('uk-margin-small');
    var inp = $('<textarea />').addClass('uk-textarea ' + cName)
              .attr('placeholder', placeholder).attr('maxlength', 500).attr('rows', rownum);
    return div.append(inp);
}
//ランダム文字列生成
function makeRndStr(){
    return Math.random().toString(32).substring(2);
}