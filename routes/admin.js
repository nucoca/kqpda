const express = require('express');
const router = express.Router();
const path = require('path');
const bcrypt = require('bcryptjs');
var db = require('../common/db')
var conf = require('../common/conf');

router.get('/', (req, res, next) =>{
    //認証チェック
    if(!isAuth(req)){
        res.redirect('/logout');
        return;
    }
    res.render('userlist', {'users': [], 
                            'user': req.session.passport.user,
                            'adminConf': conf.getAdminConf
                        })
});

router.post('/', (req, res, next) =>{
    //認証チェック
    if(!isAuth(req)){
        res.redirect('/logout');
        return;
    }
    selectUserAll(req).then(result =>{
        if(!result.result){
            console.log()
            res.render('userlist', {'message': result.message, 
                                    'user': req.session.passport.user,
                                    'adminConf': conf.getAdminConf
                            });
            return;
        }
        res.render('userlist', {'users': result.users, 
                                'user': req.session.passport.user,
                                'adminConf': conf.getAdminConf
          })
    }).catch(e =>{
        console.log(e);
        res.status(500).json({
            'message': 'サーバー内部エラー'
        });
    });
});
//登録/更新画面
router.get('/regist', (req, res, next) =>{
    //認証チェック
    if(!isAuth(req)){
        res.redirect('/logout');
        return;
    }
    var scope = req.query.scope;
    var user_id = req.query.user_id;
    if(!scope || !user_id){
        console.log('regUser 新規')
        res.render('regUser', {'user': req.session.passport.user,
                               'adminConf': conf.getAdminConf,
                               'targetUser': {},
                               'isNew': true
                            });
        return;
    }
    selectUserOne(scope, user_id).then(result =>{
        if(!result.result){
            console.log('ユーザーが存在しません。 :' + scope + ' : ' + user_id)
            res.render('userlist', {'users': [], 
                                    'user': req.session.passport.user,
                                    'adminConf': conf.getAdminConf
                                });
            return;
        }
        res.render('regUser', {'message': result.message, 
                                'user': req.session.passport.user,
                                'adminConf': conf.getAdminConf,
                                'targetUser': result.user,
                                'isNew': false
                        });
    }).catch(e =>{
        console.log(e);
        res.status(500).json({
            'message': 'サーバー内部エラー'
        });
    });
    
});


router.post('/regUser', (req, res, next) =>{
    //認証チェック
    if(!isAuth(req)){
        res.redirect('/logout');
        return;
    }
    registUserByPk(req).then(result =>{      
        if(result.result){
            res.status(200).json({
                'message': result.message
            });
        }else{
            console.log(result.message)
            res.status(500).json({
                'message': result.message
            }); 
        }
    }).catch(e =>{
        console.log(e);
        res.status(500).json({
            'message': 'サーバー内部エラー'
        });
    });
});

//認証チェック（管理者共通）
const isAuth = function(req) {
    if(!req.isAuthenticated() || !req.session.passport ){
        console.log("admin.js 未認証です");
        return false;
    }
    var user = req.session.passport.user;
    if(user.auth_type != 0) {
        console.log("admin.js 権限が一致しません");
        return false;
    }
    return true;
}
const  registUserByPk = async function(req){
    var message = '';
    var data = req.body.data;
    if(!data){
        message = 'パラメータエラー';
        console.log('regUser ' + message);
        return {'result': null, 'message': message};
    }
    if(!data.scope || !data.user_id || !data.password || !data.auth_type || !data.user_name) {
        message = '項目はすべて必須です。';
        console.log('regUser ' + message);
        return {'result': null, 'message': message};
    }
    var isNew = data.isNew;
    const client = await db.pool.connect();
    try{
        await client.query('BEGIN;');
        var result = await client.query(db.selectUserByPk, [data.scope, data.user_id]);
        var updUser = result.rows[0]
        var hashed_password = bcrypt.hashSync(data.password, 10);
        var args = [data.scope, data.user_id, hashed_password, data.auth_type, data.user_name, data.valid];
        if(isNew == 1 && updUser) {
            message = '新規登録エラー : 既に存在するアカウントです。';
            console.log('regUser ' + message);
            return {'result': null, 'message': message};
        }
        console.log("isNew : " + isNew + " updUser : " + updUser)
        if(isNew != 1 && !updUser) {
            message = '更新エラー : アカウントが存在しません。';
            console.log('regUser ' + message);
            return {'result': null, 'message': message};
        }
        //存在する場合は更新
        var sqlstr = updUser ? db.updateUser : db.inseretUser;
        result = await client.query(sqlstr, args);
        await client.query('COMMIT;')
        return {'result': result, 'message': '登録完了'};
    }catch (e){
        await client.query('ROLLBACK;');
        console.log(e);
        throw e;
    }finally{
        client.release();
    }
}
// user全データ(scope絞り込み)
const selectUserAll = async function(req){
    const client = await db.pool.connect();
    try{
        var scope = req.body.scope;
        result = await client.query(db.selectUserByScope, [scope]);
        var users = result.rows;
        return {'result': true, 
                 'users': users, 
                 'message': ''};
    }catch (e){
        console.log(e);
        throw e;
    }finally{
        client.release();
    }
}

// user1データ
const selectUserOne = async function(scope, user_id){
    const client = await db.pool.connect();
    try{
        result = await client.query(db.selectUserByPk, [scope, user_id]);
        if(!result.rows[0]){
            var message = 'userデータが存在しません。';
            console.log(message);
            return {'result': false, 'message': message};
        }
        var user = result.rows[0];
        return {'result': true, 
                 'user': user, 
                 'message': ''};
    }catch (e){
        console.log(e);
        throw e;
    }finally{
        client.release();
    }
}
module.exports = router;