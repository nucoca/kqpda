const express = require('express');
const router = express.Router();
const path = require('path');
const bcrypt = require('bcryptjs');
var bodyParser = require('body-parser');
var passport = require('passport')
  , LocalStrategy = require('passport-local').Strategy;
router.use(bodyParser.urlencoded({ extended: true }));
var db = require('../common/db')

router.get('/', function(req, res){
    //res.sendFile(path.resolve(__dirname, '..') + '/public/login.html')
  res.render('login');
});

router.get('/:scope', function(req, res){
  res.render('login',{scope: req.params.scope});
});
var authFnc = async function(username, password, cb)  {
    //routerのpassport.authenticate()が呼ばれたらここの処理が走る。
    const client = await db.pool.connect();
    try {

      if(username == process.env.ENV_ADMIN_USER && password == process.env.ENV_ADMIN_PASS ) {
        console.log("admin account login")
        return cb(null, {"userid": username, 
                        "auth_type": 0, 
                        "scope": "admin", 
                        "charactername": "管理者"});
      }
      var args = [username];
      var result = await client.query(db.selectUserByAuth, args);
      if(!result.rows || result.rows.length === 0){
        console.log('not found');
        return cb(null, null);
      }
      var row = result.rows[0];
      bcrypt.compare(password, row.password, function(err, isValid){
        if(err){
          console.log('auth error');
          return cb(null, null);
        } else{
          if(isValid){
            return cb(null, {"userid": username, 
                             "auth_type": row.auth_type, 
                             "scope": row.scope, 
                             "charactername": row.user_name})
          }
          console.log('invalid password')
          return cb(null, null);
        }
      });

    } catch(e) {
      console.log(e);
      return cb(e, null);
    } finally {
      client.release();
    }
};
var LocalStrategy = require('passport-local').Strategy;
passport.use(new LocalStrategy(
    function(username, password, cb) {
      console.log('login : ' + username)
      authFnc(username, password, function(err, user) {
        if (err) { console.log(err); return cb(err); }
        if (!user) { return cb(null, false); }
        //if (user.password != password) { return cb(null, false); }
        return cb(null, user);
      });
    }
  ));
passport.serializeUser(function(username, done) {
	console.log('serializeUser');
	done(null, username);
});
passport.deserializeUser(function(username, done) {
	console.log('deserializeUser');
	done(null, {name:username, msg:'my message'});
});
router.post('/',
 passport.authenticate('local',{
    failureRedirect: '/login',
    session: true,
 }),
 function(req, res){
   var auth_type = req.user.auth_type;
   //admin 管理者 
   if(0 === auth_type) res.redirect('admin');
   //GM権限
   else if(1 === auth_type) res.redirect('gamemaster/' + req.user.scope);
   //プレイヤー
   else res.redirect('player/' + req.user.scope);
 });
module.exports = router;