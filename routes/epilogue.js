const express = require('express');
const router = express.Router();
const path = require('path');
var db = require('../common/db');
var conf = require('../common/conf');


router.get('/:scope', (req, res) =>{

    selectPlayerAll(req).then(result =>{
        if(!result.result){
            console.log()
            res.render('epilogue', {'message': result.message, 
                              'scope': req.params.scope,
                              'conf': conf.getSysConf(req.params.scope),
                            });
            return;
        }
        res.render('epilogue', {'players': result.players, 
                              'sysdate': result.sysdate,
                              'enddate': result.enddate,
                              'nowdate': result.nowdate, // 初回は最終日がactive
                              'scope': req.params.scope,
                              'conf': conf.getSysConf(req.params.scope),
                            })
    }).catch(e =>{
        console.log(e);
        res.status(500).json({
            'message': 'サーバー内部エラー'
        });
    });
});

router.get('/:scope/:pid/:hid', (req, res) =>{

    selectPlayerHist(req).then(result =>{
        if(!result.result){
            res.render('epilogue', {'message': result.message, 
                              'scope': req.params.scope,
                              'conf': conf.getSysConf(req.params.scope),
                            });
            return;
        }
        res.render('epilogueplayer', {'player': result.player, 
                              'playerCond': result.playerCond, 
                              'playerTool': result.playerTool, 
                              'additionalconf': result.additionalconf,
                              'additional': result.additional,
                              'hcnt': result.hcnt,
                              'sysdate': result.sysdate,
                              'nowdate': result.nowdate,
                              'scope': req.params.scope,
                              'conf': conf.getSysConf(req.params.scope),
                              'history': true,})
    }).catch(e =>{
        console.log(e);
        res.status(500).json({
            'message': 'サーバー内部エラー'
        });
    });
});

// プレイヤー全データ
const selectPlayerAll = async function(req){
    const client = await db.pool.connect();
    try{
        var scope = req.params.scope;
        //システム日時
        var sysdate = await getSysdate(client, scope);
        if(sysdate.sysdate != -1){
            return {'result': false, 
                    'sysdate': sysdate.sysdate,
                    'message': 'データが存在しないか、終了前の村です'};
        }
        // 取得対象日が設定されていない場合は最終日
        var nowdate = req.query.nowdate;
        var targetDate = (!nowdate && nowdate != 0) ? sysdate.enddate : nowdate;
        //プレイヤーデータ以下 開始時点のDWHを取得
        result = await client.query(db.selectPlayerDwh, [scope, targetDate]);
        var players = result.rows;
        return {'result': true, 
                'sysdate': sysdate.sysdate,
                'enddate': sysdate.enddate,
                'nowdate': targetDate,
                'players': players, 
                 message: ''};
    }catch (e){
        console.log(e);
        throw e;
    }finally{
        client.release();
    }
}
//プレイヤー履歴取得
const selectPlayerHist = async function(req){
    
    const client = await db.pool.connect();
    try{
        var scope = req.params.scope;
        var pid = req.params.pid;
        var hid = req.params.hid;
        if(!hid){
            var message = 'selectPlayerHist　パラメータエラー';
            console.log(message);
            return {'result': false, 'message': message};
        }
        //システム日時
        var sysdate = await getSysdate(client, scope);
        if(sysdate.sysdate != -1){
            return {'result': false, 
                    'sysdate': sysdate.sysdate,
                    'message': 'データが存在しないか、終了前の村です'};
        }
        // 取得対象日が設定されていない場合は最終日
        var nowdate = req.query.nowdate;
        var targetDate = (!nowdate && nowdate != 0) ? sysdate.enddate : nowdate;
        //プレイヤーデータ履歴以下
        var args = [scope, pid, hid];
        result = await client.query(db.selectPlayerHistoryEpi, args);
        if(!result.rows[0]){
            var message = 'playerデータが存在しません。';
            console.log(message);
            return {'result': false, 'message': message};
        }
        var playerHist = result.rows[0];
        // 件数
        result = await client.query(db.selectPlayerHistoryCnt, [scope, pid]);
        var hcnt = result.rows[0].cnt;
        result = await client.query(db.selectPlayerCondHistory, args);
        var playerCond = result.rows;
        result = await client.query(db.selectPlayerToolHistory, args);
        var playerTool = result.rows;
        var res = {'result': true, 
                    'sysdate': sysdate.sysdate,
                    'nowdate': targetDate,
                    'player': playerHist, 
                    'playerCond': playerCond, 
                    'playerTool': playerTool, 
                    'hcnt' : hcnt, 
                    'message': 'selectPlayerHist OK'};
        //追加要素ある場合のみ
        var additionalconf = conf.getAdditionalConf(scope);
        if(!additionalconf){
            return res;
        }
        res.additionalconf = additionalconf;
        var additional = {};
        for(i = 0; i < additionalconf.length; i++){
            var div = additionalconf[i].div;
            args = [scope, pid, hid, div];
            result = await client.query(db.selectPlayerAdditionalHistory, args);
            additional[div] = result.rows;
        }
        res.additional =additional;
        res.result = true; //現状エラー要件なし 0件許容
        return res;
    }catch (e){
        console.log(e);
        throw e;
    }finally{
        client.release();
    }
}
//システム日時取得
const getSysdate = async function(client, scope) {
    //システム日時
    var result = await client.query(db.selectSysProgress, [scope]);
    var sysdate = !(result.rows[0]) ? 0 : result.rows[0].progress_date;
    var enddate = !(result.rows[0]) ? 0 : result.rows[0].end_date;
    return {"sysdate": sysdate, "enddate": enddate};
}
module.exports = router;