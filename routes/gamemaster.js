const express = require('express');
const router = express.Router();
const path = require('path');
var bodyParser = require('body-parser');
var db = require('../common/db');
var conf = require('../common/conf');
var moment = require("moment-timezone");
router.use(bodyParser.urlencoded({extended:true}));
router.use(bodyParser.json());
//初期表示
router.get('/', (req, res, next) =>{

    if(!isAuth(req)){
        res.redirect('/logout');
        return;
    }
    res.redirect(req.session.passport.user.scope);
});
router.get('/:scope', (req, res) =>{

    if(!isAuth(req)){
        res.redirect('/logout');
        return;
    }
    selectPlayers(req).then(result =>{
        if(result.sysdate == -1) {
            req.logout();
            res.redirect('/epilogue/' + req.params.scope);
            return;
        }
        res.render('gamemaster', {'players': result.players,  
                              'users': result.users, 
                              'sysdate': result.sysdate,
                              'scope': req.params.scope,
                              'conf': conf.getSysConf(req.params.scope),
                              'user': req.session.passport.user})
    }).catch(e =>{
        console.log(JSON.stringify(e));
        res.status(500).json({
            'message': 'サーバー内部エラー'
        });
    });
});
//プレイヤーデータ詳細
router.get('/:scope/editPlayerDetail/:pid', (req, res) =>{

    if(!isAuth(req)){
        res.redirect('/logout');
        return;
    }
    getPlayerDetail(req).then(result =>{
        //console.log(JSON.stringify(result))
        if(result.sysdate == -1) {
            req.logout();
            res.redirect('/epilogue/' + req.params.scope);
            return;
        }
        res.render('playedit', {'conds': result.conds,  
                              'tools': result.tools, 
                              'player': result.player,
                              'additionalconf': result.additionalconf,
                              'additional': result.additional,
                              'scope': req.params.scope,
                              'conf': conf.getSysConf(req.params.scope),
                              'user': req.session.passport.user})
    }).catch(e =>{
        console.log(JSON.stringify(e));
        res.status(500).json({
            'message': 'サーバー内部エラー'
        });
    });
});
//プレイヤーデータ編集
router.post('/:scope/savePlayer', (req, res, next) =>{
    if(!isAuth(req)){
        res.redirect('/logout');
        return;
    }
    savePlayerData(req).then(result =>{      
        if(result.result){
            res.status(200).json({
                'message': result.message
            });
        }else{
            console.log(result.message)
            res.status(500).json({
                'message': result.message
            }); 
        }
    }).catch(e =>{
        console.log(JSON.stringify(e));
        res.status(500).json({
            'message': 'サーバー内部エラー'
        });
    });
});

//プレイヤーデータpreview
router.post('/:scope/editPlayerDetail/preview', (req, res, next) =>{
    if(!isAuth(req)){
        res.redirect('/logout');
        return;
    }
    var jsonStr = req.body.jsondata;
    var data = JSON.parse(jsonStr);
    var scope = req.params.scope;
    res.render('preview',
              {'playerCond': data.cond,  
               'playerTool': data.tool, 
               'player': data.player,
               'additionalconf': conf.getAdditionalConf(scope),
               'additional': data.additional,
               'scope': scope,
               'conf': conf.getSysConf(scope),
               'user': req.session.passport.user})
});
//ゲーム日付進行
router.post('/:scope/commit', (req, res, next) =>{

    if(!isAuth(req)){
        res.redirect('/logout');
        return;
    }
    commitSysDate(req).then(result =>{    
        if(result.res){
            res.status(200).json({
                'sysdate': result.sysdate,
            });
        }else{
            console.log(result.message)
            res.status(500).json({
                'message': result.message
            }); 
        }
    }).catch(e =>{
        console.log(JSON.stringify(e));
        res.status(500).json({
            'message': 'サーバー内部エラー'
        });
    });
});
//ゲーム日付終了処理
router.post('/:scope/endcommit', (req, res, next) =>{

    if(!isAuth(req)){
        res.redirect('/logout');
        return;
    }
    endcommit(req).then(result =>{    
        if(result.res){
            res.status(200).json({
                'result': true,
                'sysdate': result.sysdate,
            });
        }else{
            res.status(500).json({
                'result': false,
                'message': result.message
            }); 
        }
    }).catch(e =>{
        console.log(JSON.stringify(e));
        res.status(500).json({
            'message': 'サーバー内部エラー'
        });
    });
});
//プレイヤーデータ詳細編集
router.post('/:scope/editPlayerDetail/save', (req, res, next) =>{
    if(!isAuth(req)){
        res.redirect('/logout');
        return;
    }
    savePlayerDataDetail(req).then(result =>{      
        if(result.result){
            res.status(200).json({
                'message': result.message
            });
        }else{
            console.log(result.message)
            res.status(500).json({
                'message': result.message
            }); 
        }
    }).catch(e =>{
        console.log(JSON.stringify(e));
        res.status(500).json({
            'message': 'サーバー内部エラー'
        });
    });
});
//認証チェック（GM共通）
const isAuth = function(req) {
    if(!req.isAuthenticated() || !req.session.passport ){
        console.log("gamemaster.js 未認証です");
        return false;
    }
    var user = req.session.passport.user;
    if(user.auth_type != 1) {
        console.log("gamemaster.js 権限が一致しません");
        return false;
    }
    return true;
}
//プレイヤーデータ取得
const  selectPlayers = async function(req){
    const client = await db.pool.connect();
    var message = '';
    try{
        var user = req.session.passport.user;
        var result = await client.query(db.selectSysProgress, [user.scope]);
        if(!result.rows[0]){
            message = 'システム日時が設定されていません';
            console.log(message);
            return {'result': false, 'message': message};
        }
        var sysdate = result.rows[0].progress_date
        if(sysdate == -1) {
            return {'sysdate': sysdate}
        }
        result = await client.query(db.selectPlayers, [user.scope]);
        var players = result.rows;
        // 有効化ユーザーのみ
        result = await client.query(db.selectUsers, [user.scope]);
        var users = result.rows;
        return {'result': true, 'sysdate': sysdate, 'players': players, 'users': users, 'message': message};
    }catch (e){
        console.log(JSON.stringify(e));
        throw e;
    }finally{
        client.release();
    }
}
//プレイヤーデータ編集
const  savePlayerData = async function(req){
    const client = await db.pool.connect();
    var players = req.body.players;
    if(!players || players.length == 0){
        return {'result': true, 'message': '対象なし'};
    }
    var sysdate = req.body.sysdate;
    try{
        await client.query('BEGIN;');
        var sysdate = await getSysDate(client, req.params.scope);
        for(const i  in players){
            var player = players[i];
            var histno = 0;
            //履歴 開始中のみ
            var expire_datetime = sysdate > 0 ?  moment().tz("Asia/Tokyo").format('YYYY/MM/DD HH:mm:ss') : '';
            if(sysdate > 0){
                histRes = await makeHistory(client, req.params.scope, player.player_id, sysdate, expire_datetime);
                if(!histRes) {
                    return {'result': false, 'message': '更新エラー'};
                }
                histno = parseInt(histRes.histno);
            }
            var args = [player.identify1, player.identify2, player.user_id, player.info, req.params.scope,
                        player.player_id, histno, expire_datetime, player.death, player.cause];
            var result = await client.query(db.updatePlayer, args);
            if(result.rowCount < 1) {
                await client.query('ROLLBACK;');
                return {'result': false, 'message': '更新エラー'};
            }
        }
        await client.query('COMMIT;')
        return {'result': true, 'message': '処理結果:OK'};
    }catch (e){
        console.log(JSON.stringify(e));
        await client.query('ROLLBACK;');
        throw e;
    }finally{
        client.release();
    }
}
//ゲーム進行処理
const  commitSysDate = async function(req){
    var sysdate = req.body.sysdate;
    if(!sysdate && sysdate != 0){
        return {'res': false, 'message': 'パラメータ不正: sysdate'};
    }
    const client = await db.pool.connect();
    try{
        await client.query('BEGIN;');
        var makeDwhRes = await makeDwh(req, sysdate);
        if(!makeDwhRes.res) {
            return {'res': false, 'message':  '日替わり処理失敗'};
        }
        var newdate = ++sysdate;
        var args = [newdate, req.params.scope, 0];
        var result = await client.query(db.updateSysProgress, args);
        if(result.rowCount != 1) {
            await client.query('ROLLBACK;');
            return {'res': false, 'message':  '更新エラー'};
        }
        await client.query('COMMIT;')
        return {'res': true, 'sysdate': newdate};
    }catch (e){
        console.log(JSON.stringify(e));
        await client.query('ROLLBACK;');
        throw e;
    }finally{
        client.release();
    }
}
//プレイヤーデータ詳細取得
const getPlayerDetail = async function(req){
    const client = await db.pool.connect();
    var pid = req.params.pid;
    if(!pid) {
        var message = 'getPlayerCond パラメータ不正: pid';
        console.log(message);
        return {'result': false, 'message': message};
    }
    try{
        var user = req.session.passport.user;
        var sysdate = await getSysDate(client, user.scope);
        if(sysdate == -1) {
            return {'sysdate': sysdate}
        }
        var args = [user.scope, pid]
        var result = await client.query(db.selectPlayerByPid, args);
        // TODO 0件時処理
        var player = result.rows[0];
        // 詳細取得
        var res = await getPlayerdataAddAll(client, user.scope, pid);
        if(!res) {
            res.result = false;
            return res;
        }
        res.player = player;
        res.result = true;
        res.sysdate = sysdate;
        res.message = null;
        return res;
    }catch (e){
        console.log(JSON.stringify(e));
        throw e;
    }finally{
        client.release();
    }
}
//プレイヤーデータ詳細編集
const savePlayerDataDetail = async function(req){
    const client = await db.pool.connect();
    var player = req.body.player
    var cond = req.body.cond;
    var tool = req.body.tool;
    var player_id = req.body.player_id;
    var scope = req.params.scope;
    if(!player_id || !cond || !tool || !player || !scope){
        return {'result': false, 'message': 'パラメータ不正'};
    }
    try{
        await client.query('BEGIN;');
        var sysdate = await getSysDate(client, scope);
        var histno = 0;
        //履歴 開始中のみ
        var expire_datetime = sysdate > 0 ?  moment().tz("Asia/Tokyo").format('YYYY/MM/DD HH:mm:ss') : '';
        if(sysdate > 0){
            histRes = await makeHistory(client, scope, player_id, sysdate, expire_datetime);
            if(!histRes) {
                return {'result': false, 'message': '更新エラー'};
            }
            histno = parseInt(histRes.histno);
        }
        var updargs = [player.identify1, player.identify2, player.user_id, player.info,
                       scope, player_id, histno, expire_datetime, player.death, player.cause];
        var result = await client.query(db.updatePlayer, updargs);
        if(result.rowCount < 1) {
            await client.query('ROLLBACK;');
            return {'result': false, 'message': '更新エラー'};
        }
        var delargs = [scope, player_id];
        //削除
        await client.query(db.deletePlayerCond, delargs);
        await client.query(db.deletePlayerTool, delargs);
        for(i = 0; i < cond.length; i++){
            var item = cond[i];
            var args = [scope, player_id, i + 1, item.valid, item.cond, item.exp ];
            result = await client.query(db.insertPlayerCond, args);
            if(result.rowCount < 1) {
                await client.query('ROLLBACK;');
                return {'result': false, 'message': '更新エラー'};
            }
        }
        for(i = 0; i < tool.length; i++){
            var item = tool[i];
            var args = [scope, player_id, i + 1, item.valid, item.name, item.info, item.exp ];
            var result = await client.query(db.insertPlayerTool, args);
            if(result.rowCount < 1) {
                await client.query('ROLLBACK;');
                return {'result': false, 'message': '更新エラー'};
            }
        }
        //追加要素ある場合のみ
        var additionalconf = conf.getAdditionalConf(scope);
        var additionalitems = req.body.additional;
        if(!additionalconf || !additionalitems || additionalitems.length == 0){
            await client.query('COMMIT;')
            return {'result': true, 'message': '処理結果:OK'};
        }
        for(i = 0; i < additionalconf.length; i++) {
            var divname = additionalconf[i].div;
            var itemlist = additionalitems[divname];
            //区分ごと削除
            delargs = [scope, player_id, divname];
            await client.query(db.deletePlayerAdditional, delargs);
            for(k = 0; k < itemlist.length; k++) {
                var item = itemlist[k];
                var args = [scope, player_id, divname, k + 1, item.valid, item.title, item.info, item.exp ];
                result = await client.query(db.insertPlayerAdditional, args);
                if(result.rowCount < 1) {
                    await client.query('ROLLBACK;');
                    return {'result': false, 'message': '更新エラー'};
                }
            }
        }
        await client.query('COMMIT;')
        return {'result': true, 'message': '処理結果:OK'};
    }catch (e){
        console.log(JSON.stringify(e));
        await client.query('ROLLBACK;');
        throw e;
    }finally{
        client.release();
    }
}

// 日替わり時断面作成(N日目終了時) トランザクションは呼び出し元全体
const makeDwh = async function(req, sysdate){
    const client = await db.pool.connect();
    try{
        result = await client.query(db.selectPlayers, [req.params.scope]);
        // TODO 0件時処理
        var players = result.rows;
        for(const i in players){
            var player = players[i];
            var histno = parseInt(player.now_hist_no) + 1; // エピローグ時参照するのはnow_hist_no+1の履歴
            // ※システム日時が0の場合は開始前
            var updargs = [req.params.scope, sysdate, player.player_id, player.role, player.role_disp,
                player.identify1, player.identify2, player.cond, player.info, player.death, player.cause
                , histno, player.user_name];
            var result = await client.query(db.insertPlayerDwh, updargs);
            if(result.rowCount < 1) {
                console.log('makeDwh 登録エラー')
                await client.query('ROLLBACK;');
                return {'res': false, 'message': 'makeDwh 登録エラー'};
            }
        }
        return {'res': true};
    }catch (e){
        console.log(JSON.stringify(e));
        await client.query('ROLLBACK;');
        throw e;
    }
}
//履歴作成 return now_hist_no トランザクション範囲、エラーハンドリングは呼び出し元で
const makeHistory = async function(client, scope, player_id, sysdate, expire_datetime){
    //現データ取得 
    var args = [scope, player_id]
    var result = await client.query(db.selectPlayerByPid, args);
    // TODO 0件時処理
    var player = result.rows[0];
    var histno = parseInt(player.now_hist_no) + 1;
    var details = await getPlayerdataAddAll(client, scope, player_id);
    var pargs = [scope, player_id, histno, player.role, player.role_disp, 
                 player.identify1, player.identify2, player.info, player.user_id, expire_datetime, sysdate, player.valid_from
                ,player.death ,player.cause];
    var result = await client.query(db.insertPlayerHist, pargs);
    if(result.rowCount < 1) {
        await client.query('ROLLBACK;');
        return null;
    }
    // 条件
    if(details.conds){
        for(i = 0; i < details.conds.length; i++) {
            var item = details.conds[i];
            var args = [scope, player_id, histno, item.cond_no, item.valid, item.cond, item.exp];
            result = await client.query(db.insertPlayerCondHist, args);
            if(result.rowCount < 1) {
                await client.query('ROLLBACK;');
                return null;
            }
        }
    }
    // ツール
    if(details.tools){
        for(i = 0; i < details.tools.length; i++) {
            var item = details.tools[i];
            var args = [scope, player_id, histno, item.tool_no, item.valid, item.name, item.info, item.exp];
            result = await client.query(db.insertPlayerToolHist, args);
            if(result.rowCount < 1) {
                await client.query('ROLLBACK;');
                return null;
            }
        }
    }
    // 追加要素
    if(details.additionalconf && details.additional){
        for (k = 0; k < details.additionalconf.length; k ++) {
            var divname = details.additionalconf[k].div
            var additionals = details.additional[divname];
            if(!additionals) continue;
            for(i = 0; i < additionals.length; i++) {
                var item = additionals[i];
                var args = [scope, player_id, histno, divname, item.add_no, item.valid, item.title, item.info, item.exp];
                result = await client.query(db.insertPlayerAddHist, args);
                if(result.rowCount < 1) {
                    await client.query('ROLLBACK;');
                    return null;
                }
            }
        }
    }
    return {"histno": histno}
}
//進行日時取得
const getSysDate = async function(client, scope) {
    var result = await client.query(db.selectSysProgress, [scope]);
    var sysdate = !(result.rows[0]) ? 0 : result.rows[0].progress_date;
    return sysdate;
}
// 条件・ツール等データ取得 by pid
const getPlayerdataAddAll = async function(client, scope, player_id) {
    var res = {};
    var args = [scope, player_id]
    var result = await client.query(db.selectPlayerCondAll, args);
    var conds = result.rows;
    result = await client.query(db.selectPlayerToolAll, args);
    var tools = result.rows;
    res.conds = conds;
    res.tools = tools;
    var additionalconf = conf.getAdditionalConf(scope);
    
    if(!additionalconf || additionalconf.length == 0) return res;
    //追加要素ありの場合
    res.additionalconf = additionalconf;
    var additional = {};
    for(i = 0; i < additionalconf.length; i++){
        var div = additionalconf[i].div;
        args = [scope, player_id, div];
        result = await client.query(db.selectPlayerAdditionalAll, args);
        additional[div] = result.rows;
    }
    res.additional = additional;
    return res;
}
//ゲーム終了処理
const  endcommit = async function(req){
    var sysdate = req.body.sysdate;
    if(!sysdate && sysdate < 1){
        return {'res': false, 'message': 'パラメータ不正: sysdate'};
    }
    const client = await db.pool.connect();
    try{
        await client.query('BEGIN;');
        // 日替わり断面作成
        var makeDwhRes = await makeDwh(req, sysdate);
        if(!makeDwhRes.res) {
            return {'res': false, 'message':  '日替わり処理失敗'};
        }
        console.log('endcommit makeDwh : OK')
        var args = [-1, req.params.scope, sysdate];
        var result = await client.query(db.updateSysProgress, args);
        if(result.rowCount != 1) {
            await client.query('ROLLBACK;');
            return {'res': false, 'message':  '更新エラー'};
        }
        //最新をすべて履歴に移す
        var expire_datetime =  moment().tz("Asia/Tokyo").format('YYYY/MM/DD HH:mm:ss');
        var pids = req.body.pids;
        for(const i  in pids){
            var pid = pids[i];
            console.log("endcommit makeHistory : " + pid)
            histRes = await makeHistory(client, req.params.scope, pid, sysdate, expire_datetime);
            if(!histRes) {
                return {'result': false, 'message': '更新エラー'};
            }
        }
        await client.query('COMMIT;')
        return {'res': true, 'sysdate': sysdate};
    }catch (e){
        console.log(JSON.stringify(e));
        await client.query('ROLLBACK;');
        throw e;
    }finally{
        client.release();
    }
}
module.exports = router;