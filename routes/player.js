const express = require('express');
const router = express.Router();
const path = require('path');
var db = require('../common/db');
var conf = require('../common/conf');

router.get('/', (req, res, next) =>{
    if(!isAuth(req)){
        res.redirect('/logout');
        return;
    }
    res.redirect(req.session.passport.user.scope);
});

router.get('/:scope', (req, res) =>{
    if(!isAuth(req)){
        res.redirect('/logout');
        return;
    }
    selectPlayerByUser(req).then(result =>{
        if(result.sysdate == -1) {
            req.logout();
            res.redirect('/epilogue/' + req.params.scope);
            return;
        }
        res.render('player', {'player': result.player, 
                              'playerCond': result.playerCond, 
                              'playerTool': result.playerTool, 
                              'additionalconf': result.additionalconf,
                              'additional': result.additional,
                              'sysdate': result.sysdate,
                              'scope': req.params.scope,
                              'conf': conf.getSysConf(req.params.scope),
                              'user': req.session.passport.user})
    }).catch(e =>{
        console.log(e);
        res.status(500).json({
            'message': 'サーバー内部エラー'
        });
    });
});
router.get('/:scope/history/:hid', (req, res) =>{
    if(!isAuth(req)){
        res.redirect('/logout');
        return;
    }
    selectPlayerHistByUser(req).then(result =>{
        if(result.sysdate == -1) {
            req.logout();
            res.redirect('/epilogue/' + req.params.scope);
            return;
        }
        res.render('player', {'player': result.player, 
                              'playerCond': result.playerCond, 
                              'playerTool': result.playerTool, 
                              'additionalconf': result.additionalconf,
                              'additional': result.additional,
                              'sysdate': result.sysdate,
                              'scope': req.params.scope,
                              'user': req.session.passport.user,
                              'conf': conf.getSysConf(req.params.scope),
                              'history': true,})
    }).catch(e =>{
        console.log(e);
        res.status(500).json({
            'message': 'サーバー内部エラー'
        });
    });
});

//認証チェック（Player共通）
const isAuth = function(req) {
    if(!req.isAuthenticated() || !req.session.passport ){
        console.log('player.js unAuthenticated');
        return false;
    }
    var user = req.session.passport.user;
    if(user.auth_type != 2) {
        console.log('invalid auth_type : redirect logout');
        return false;
    }
    return true;
}
//ログインユーザーのプレイヤーデータ最新取得
const selectPlayerByUser = async function(req){
    const client = await db.pool.connect();
    try{
        var user = req.session.passport.user;
        //システム日時
        var sysdate = await getSysdate(client, user.scope);
        if(sysdate == -1) {
            return {'sysdate': sysdate}
        } else if(sysdate == 0){
            return {'result': true, 
                    'sysdate': sysdate,
                    'player': null, 
                    'playerCond': null, 
                    'playerTool': null, 
                    'message': 'playerログイン'};
        }
        //プレイヤーデータ以下
        result = await client.query(db.selectPlayer, [user.scope, user.userid]);
        if(!result.rows[0]){
            var message = 'selectPlayerByUser playerデータが存在しません。';
            console.log(message);
            return {'result': false, 'message': message};
        }
        var player = result.rows[0];
        var args = [user.scope, player.player_id];
        result = await client.query(db.selectPlayerCond, args);
        var playerCond = result.rows;
        result = await client.query(db.selectPlayerTool, args);
        var playerTool = result.rows;
        var res = {'result': true, 
                    'sysdate': sysdate,
                    'player': player, 
                    'playerCond': playerCond, 
                    'playerTool': playerTool, 
                    'message': 'playerログイン'};
        //追加要素ある場合のみ
        var additionalconf = conf.getAdditionalConf(user.scope);
        if(!additionalconf){
            return res;
        }
        res.additionalconf = additionalconf;
        var additional = {};
        for(i = 0; i < additionalconf.length; i++){
            var div = additionalconf[i].div;
            args = [user.scope, player.player_id, div];
            result = await client.query(db.selectPlayerAdditional, args);
            additional[div] = result.rows;
        }
        res.additional =additional;
        res.result = true; //現状エラー要件なし 0件許容
        return res;
    }catch (e){
        console.log(e);
        throw e;
    }finally{
        client.release();
    }
}
//ログインユーザー履歴取得
const selectPlayerHistByUser = async function(req){
    const client = await db.pool.connect();
    try{
        var user = req.session.passport.user;
        var hid = req.params.hid;
        if(!hid){
            var message = 'selectPlayerHistByUser　パラメータエラー';
            console.log(message);
            return {'result': false, 'message': message};
        }
        //システム日時
        var sysdate = await getSysdate(client, user.scope);
        if(sysdate == -1) {
            return {'sysdate': sysdate}
        } else if(sysdate == 0){
            return {'result': true, 
                    'sysdate': sysdate,
                    'player': null, 
                    'playerCond': null, 
                    'playerTool': null, 
                    'message': 'playerログイン'};
        }
        //プレイヤーデータ以下
        var result = await client.query(db.selectPlayer, [user.scope, user.userid]);
        if(!result.rows[0]){
            var message = 'selectPlayerHistByUser playerデータが存在しません。';
            console.log(message);
            return {'result': false, 'message': message};
        }
        var player = result.rows[0];
        //プレイヤーデータ履歴以下
        var args = [user.scope, player.player_id, hid];
        result = await client.query(db.selectPlayerHistory, args);
        if(!result.rows[0]){
            var message = 'playerデータが存在しません。';
            console.log(message);
            return {'result': false, 'message': message};
        }
        var playerHist = result.rows[0];
        playerHist.now_hist_no = player.now_hist_no;
        result = await client.query(db.selectPlayerCondHistory, args);
        var playerCond = result.rows;
        result = await client.query(db.selectPlayerToolHistory, args);
        var playerTool = result.rows;
        var res = {'result': true, 
                    'sysdate': sysdate,
                    'player': playerHist, 
                    'playerCond': playerCond, 
                    'playerTool': playerTool, 
                    'message': 'playerログイン'};
        //追加要素ある場合のみ
        var additionalconf = conf.getAdditionalConf(user.scope);
        if(!additionalconf){
            return res;
        }
        res.additionalconf = additionalconf;
        var additional = {};
        for(i = 0; i < additionalconf.length; i++){
            var div = additionalconf[i].div;
            args = [user.scope, player.player_id, hid, div];
            result = await client.query(db.selectPlayerAdditionalHistory, args);
            additional[div] = result.rows;
        }
        res.additional =additional;
        res.result = true; //現状エラー要件なし 0件許容
        return res;
    }catch (e){
        console.log(e);
        throw e;
    }finally{
        client.release();
    }
}
//システム日時取得
const getSysdate = async function(client, scope) {
    //システム日時
    var result = await client.query(db.selectSysProgress, [scope]);
    var sysdate = !(result.rows[0]) ? 0 : result.rows[0].progress_date;
    return sysdate;
}
module.exports = router;