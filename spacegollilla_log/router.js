const express = require('express');
const router = express.Router();
const path = require('path');
const pass01 = 'mmgollilla2008'
const result01 = ['main_log','sec_a-b','sec_a-c','sec_a-d','sec_b-c','sec_b-d','sec_c-d'];
const result01disp = ['全体ログ','密談_a-b','密談_a-c','密談_a-d','密談_b-c','密談_b-d','密談_c-d'];
router.get('/', (req, res, next) =>{
    res.sendFile(__dirname + '/index.html');
});

router.post('/auth', (req, res, next) =>{
    var password = req.body.password;
    var err = 'パスワードが違います';
    var result = [];
    if(password == pass01){
        result = result01;
        err = null;
    }
    res.status(200).json({
        'list': result,
        'listdisp': result01disp,
        'err' : err
    });
});
router.get('/page/:pagename', (req, res, next) =>{
    res.sendFile(__dirname + '/' + req.params.pagename + '.html')
});
module.exports = router;