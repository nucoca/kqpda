-- Project Name : noname
-- Date/Time    : 2021/05/05 18:53:33
-- Author       : nucoc
-- RDBMS Type   : PostgreSQL
-- Application  : A5:SQL Mk-2

-- t_player_history
DROP TABLE if exists t_player_history CASCADE;

CREATE TABLE t_player_history (
  scope character varying(10) NOT NULL
  , player_id integer NOT NULL
  , hist_no integer NOT NULL
  , role character varying(60)
  , role_disp character varying(60)
  , identify1 character varying(60)
  , identify2 character varying(60)
  , info text
  , user_id character varying(30)
  , expire_datetime character varying(30)
  , expire_progress_date integer
  , valid_from character varying(30)
  , death integer DEFAULT 0
  , cause text
  , CONSTRAINT t_player_history_PKC PRIMARY KEY (scope,player_id,hist_no)
) ;

-- m_system_progress
DROP TABLE if exists m_system_progress CASCADE;

CREATE TABLE m_system_progress (
  scope character varying(10) NOT NULL
  , progress_date integer DEFAULT 0
  , end_date integer DEFAULT 0
  , CONSTRAINT m_system_progress_PKC PRIMARY KEY (scope)
) ;

-- m_user
DROP TABLE if exists m_user CASCADE;

CREATE TABLE m_user (
  scope character varying(10) NOT NULL
  , user_id character varying(30) NOT NULL
  , password text
  , valid_from date
  , valid_to date
  , auth_type integer
  , user_name character varying(40)
  , CONSTRAINT m_user_PKC PRIMARY KEY (scope,user_id)
) ;

-- t_player
DROP TABLE if exists t_player CASCADE;

CREATE TABLE t_player (
  scope character varying(10) NOT NULL
  , player_id integer NOT NULL
  , role character varying(60)
  , role_disp character varying(60)
  , identify1 character varying(60)
  , identify2 character varying(60)
  , info text
  , user_id character varying(30)
  , now_hist_no integer DEFAULT 0
  , valid_from character varying(30)
  , death integer DEFAULT 0
  , cause text
  , CONSTRAINT t_player_PKC PRIMARY KEY (scope,player_id)
) ;

-- t_player_additional
DROP TABLE if exists t_player_additional CASCADE;

CREATE TABLE t_player_additional (
  scope character varying(10) NOT NULL
  , player_id integer NOT NULL
  , add_div character varying(10) NOT NULL
  , add_no integer NOT NULL
  , valid integer
  , title character varying(100)
  , info text
  , exp text
  , CONSTRAINT t_player_additional_PKC PRIMARY KEY (scope,player_id,add_div,add_no)
) ;

-- t_player_additional_history
DROP TABLE if exists t_player_additional_history CASCADE;

CREATE TABLE t_player_additional_history (
  scope character varying(10) NOT NULL
  , player_id integer NOT NULL
  , hist_no integer NOT NULL
  , add_div character varying(10) NOT NULL
  , add_no integer NOT NULL
  , valid integer
  , title character varying(100)
  , info text
  , exp text
  , CONSTRAINT t_player_additional_history_PKC PRIMARY KEY (scope,player_id,hist_no,add_div,add_no)
) ;

-- t_player_cond
DROP TABLE if exists t_player_cond CASCADE;

CREATE TABLE t_player_cond (
  scope character varying(10) NOT NULL
  , player_id integer NOT NULL
  , cond_no integer NOT NULL
  , valid integer
  , cond text
  , exp text
  , CONSTRAINT t_player_cond_PKC PRIMARY KEY (scope,player_id,cond_no)
) ;

-- t_player_cond_history
DROP TABLE if exists t_player_cond_history CASCADE;

CREATE TABLE t_player_cond_history (
  scope character varying(10) NOT NULL
  , player_id integer NOT NULL
  , hist_no integer NOT NULL
  , cond_no integer NOT NULL
  , valid integer
  , cond text
  , exp text
  , CONSTRAINT t_player_cond_history_PKC PRIMARY KEY (scope,player_id,hist_no,cond_no)
) ;

-- t_player_tool
DROP TABLE if exists t_player_tool CASCADE;

CREATE TABLE t_player_tool (
  scope character varying(10) NOT NULL
  , player_id integer NOT NULL
  , tool_no integer NOT NULL
  , valid integer
  , name character varying(100)
  , info text
  , exp text
  , CONSTRAINT t_player_tool_PKC PRIMARY KEY (scope,player_id,tool_no)
) ;

-- t_player_tool_history
DROP TABLE if exists t_player_tool_history CASCADE;

CREATE TABLE t_player_tool_history (
  scope character varying(10) NOT NULL
  , player_id integer NOT NULL
  , hist_no integer NOT NULL
  , tool_no integer NOT NULL
  , valid integer
  , name character varying(100)
  , info text
  , exp text
  , CONSTRAINT t_player_tool_history_PKC PRIMARY KEY (scope,player_id,hist_no,tool_no)
) ;
-- t_player_dwh
DROP TABLE if exists t_player_dwh CASCADE;

CREATE TABLE t_player_dwh (
  scope character varying(10) NOT NULL
  , sys_date integer NOT NULL
  , player_id integer NOT NULL
  , role character varying(60)
  , role_disp character varying(60)
  , identify1 character varying(60)
  , identify2 character varying(60)
  , cond text
  , info text
  , death integer
  , cause text
  , hist_no integer
  , user_name character varying(40)
  , CONSTRAINT t_player_dwh_PKC PRIMARY KEY (scope,sys_date,player_id)
) ;

COMMENT ON TABLE t_player_history IS 't_player_history';
COMMENT ON COLUMN t_player_history.scope IS 'scope';
COMMENT ON COLUMN t_player_history.player_id IS 'player_id';
COMMENT ON COLUMN t_player_history.hist_no IS 'hist_no';
COMMENT ON COLUMN t_player_history.role IS 'role:役職';
COMMENT ON COLUMN t_player_history.role_disp IS 'role_disp:役職（表示用名称）';
COMMENT ON COLUMN t_player_history.identify1 IS 'identify1:Noなど1';
COMMENT ON COLUMN t_player_history.identify2 IS 'identify2:Noなど2';
COMMENT ON COLUMN t_player_history.info IS 'info:説明文（あなたは犯人役です、など）';
COMMENT ON COLUMN t_player_history.user_id IS 'user_id:半角英数のみ';
COMMENT ON COLUMN t_player_history.expire_datetime IS 'expire_datetime:YYYY/MM/DD HH:mm:ss形式';
COMMENT ON COLUMN t_player_history.expire_progress_date IS 'expire_progress_date:変更時の進行日数';

COMMENT ON TABLE m_system_progress IS 'm_system_progress:システム進行状況';
COMMENT ON COLUMN m_system_progress.scope IS 'scope';
COMMENT ON COLUMN m_system_progress.progress_date IS 'progress_date:進行日時';

COMMENT ON TABLE m_user IS 'm_user';
COMMENT ON COLUMN m_user.scope IS 'scope';
COMMENT ON COLUMN m_user.user_id IS 'user_id';
COMMENT ON COLUMN m_user.password IS 'password';
COMMENT ON COLUMN m_user.valid_from IS 'valid_from';
COMMENT ON COLUMN m_user.valid_to IS 'valid_to';
COMMENT ON COLUMN m_user.auth_type IS 'auth_type';
COMMENT ON COLUMN m_user.user_name IS 'user_name';

COMMENT ON TABLE t_player IS 't_player';
COMMENT ON COLUMN t_player.scope IS 'scope';
COMMENT ON COLUMN t_player.player_id IS 'player_id';
COMMENT ON COLUMN t_player.role IS 'role:役職';
COMMENT ON COLUMN t_player.role_disp IS 'role_disp:役職（表示用名称）';
COMMENT ON COLUMN t_player.identify1 IS 'identify1:Noなど1';
COMMENT ON COLUMN t_player.identify2 IS 'identify2:Noなど2';
COMMENT ON COLUMN t_player.info IS 'info:説明文（あなたは犯人役です、など）';
COMMENT ON COLUMN t_player.user_id IS 'user_id:半角英数のみ';
COMMENT ON COLUMN t_player.now_hist_no IS 'now_hist_no:現在の履歴番号';

COMMENT ON TABLE t_player_additional IS 't_player_additional';
COMMENT ON COLUMN t_player_additional.scope IS 'scope:半角英数のみ';
COMMENT ON COLUMN t_player_additional.player_id IS 'player_id';
COMMENT ON COLUMN t_player_additional.add_div IS 'add_div:追加要素区分';
COMMENT ON COLUMN t_player_additional.add_no IS 'add_no:追加要素インデックス';
COMMENT ON COLUMN t_player_additional.valid IS 'valid:0:無効 1:有効';
COMMENT ON COLUMN t_player_additional.title IS 'title:タイトル';
COMMENT ON COLUMN t_player_additional.info IS 'info:本文';
COMMENT ON COLUMN t_player_additional.exp IS 'exp:補足説明';

COMMENT ON TABLE t_player_additional_history IS 't_player_additional_history';
COMMENT ON COLUMN t_player_additional_history.scope IS 'scope';
COMMENT ON COLUMN t_player_additional_history.player_id IS 'player_id';
COMMENT ON COLUMN t_player_additional_history.hist_no IS 'hist_no';
COMMENT ON COLUMN t_player_additional_history.add_div IS 'add_div';
COMMENT ON COLUMN t_player_additional_history.add_no IS 'add_no';
COMMENT ON COLUMN t_player_additional_history.valid IS 'valid';
COMMENT ON COLUMN t_player_additional_history.title IS 'title';
COMMENT ON COLUMN t_player_additional_history.info IS 'info';
COMMENT ON COLUMN t_player_additional_history.exp IS 'exp';

COMMENT ON TABLE t_player_cond IS 't_player_cond';
COMMENT ON COLUMN t_player_cond.scope IS 'scope:半角英数のみ';
COMMENT ON COLUMN t_player_cond.player_id IS 'player_id';
COMMENT ON COLUMN t_player_cond.cond_no IS 'cond_no:インデックス';
COMMENT ON COLUMN t_player_cond.valid IS 'valid:0:無効 1:有効';
COMMENT ON COLUMN t_player_cond.cond IS 'cond:条件本文';
COMMENT ON COLUMN t_player_cond.exp IS 'exp:条件補足説明';

COMMENT ON TABLE t_player_cond_history IS 't_player_cond_history';
COMMENT ON COLUMN t_player_cond_history.scope IS 'scope';
COMMENT ON COLUMN t_player_cond_history.player_id IS 'player_id';
COMMENT ON COLUMN t_player_cond_history.hist_no IS 'hist_no';
COMMENT ON COLUMN t_player_cond_history.cond_no IS 'cond_no';
COMMENT ON COLUMN t_player_cond_history.valid IS 'valid';
COMMENT ON COLUMN t_player_cond_history.cond IS 'cond';
COMMENT ON COLUMN t_player_cond_history.exp IS 'exp';

COMMENT ON TABLE t_player_tool IS 't_player_tool';
COMMENT ON COLUMN t_player_tool.scope IS 'scope:半角英数のみ';
COMMENT ON COLUMN t_player_tool.player_id IS 'player_id';
COMMENT ON COLUMN t_player_tool.tool_no IS 'tool_no:インデックス';
COMMENT ON COLUMN t_player_tool.valid IS 'valid:0:無効 1:有効';
COMMENT ON COLUMN t_player_tool.name IS 'name:ツール名';
COMMENT ON COLUMN t_player_tool.info IS 'info:ツール本文';
COMMENT ON COLUMN t_player_tool.exp IS 'exp:ツール補足説明';

COMMENT ON TABLE t_player_tool_history IS 't_player_tool_history';
COMMENT ON COLUMN t_player_tool_history.scope IS 'scope';
COMMENT ON COLUMN t_player_tool_history.player_id IS 'player_id';
COMMENT ON COLUMN t_player_tool_history.hist_no IS 'hist_no';
COMMENT ON COLUMN t_player_tool_history.tool_no IS 'tool_no';
COMMENT ON COLUMN t_player_tool_history.valid IS 'valid';
COMMENT ON COLUMN t_player_tool_history.name IS 'name';
COMMENT ON COLUMN t_player_tool_history.info IS 'info';
COMMENT ON COLUMN t_player_tool_history.exp IS 'exp';

COMMENT ON TABLE t_player_dwh IS 't_player_dwh';
COMMENT ON COLUMN t_player_dwh.scope IS 'scope';
COMMENT ON COLUMN t_player_dwh.sys_date IS 'sys_date';
COMMENT ON COLUMN t_player_dwh.player_id IS 'player_id';
COMMENT ON COLUMN t_player_dwh.role IS 'role:役職';
COMMENT ON COLUMN t_player_dwh.role_disp IS 'role_disp:役職（表示用名称）';
COMMENT ON COLUMN t_player_dwh.identify1 IS 'identify1:Noなど1';
COMMENT ON COLUMN t_player_dwh.identify2 IS 'identify2:Noなど2';
COMMENT ON COLUMN t_player_dwh.cond IS 'cond:条件（複数ある場合は1つのみ）';
COMMENT ON COLUMN t_player_dwh.info IS 'info:説明文（あなたは犯人役です、など）';
COMMENT ON COLUMN t_player_dwh.death IS 'death';
COMMENT ON COLUMN t_player_dwh.cause IS 'cause';
COMMENT ON COLUMN t_player_dwh.hist_no IS 'hist_no:対応する履歴番号';
