delete from t_player_history where scope = 'sss';
delete from t_player_cond_history where scope = 'sss';
delete from t_player_tool_history where scope = 'sss';
delete from t_player_additional_history where scope = 'sss';
delete from t_player_dwh where scope = 'sss';
update m_system_progress set progress_date = 0 , end_date = 0 where scope = 'sss';
update t_player set now_hist_no = 0, death = 0, cause = null, valid_from = null  where scope = 'sss';
